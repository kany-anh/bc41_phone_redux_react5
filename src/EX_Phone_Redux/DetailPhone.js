import React, { Component } from "react";
import { connect } from "react-redux";

class DetailPhone extends Component {
  render() {
    let {
      maSP,
      tenSP,
      manHinh,
      heDieuHanh,
      cameraTruoc,
      cameraSau,
      ram,
      rom,
      giaBan,
      hinhAnh,
    } = this.props.changePhone;
    return (
      <div className="row">
        <div className="col-4">
          <img src={hinhAnh} alt="" />
        </div>
        <div className="col-2 mt-5  mt-5">
          <h5>Mã sản phẩm:</h5>
          <h5>Tên sản phẩm:</h5>
          <h5>Màn hình:</h5>
          <h5>Hệ điều hành:</h5>
          <h5>Camera Trước:</h5>
          <h5>Camera Sau:</h5>
          <h5>Ram:</h5>
          <h5>Bộ nhớ trong:</h5>
          <h5>Giá bán:</h5>
        </div>
        <div className="col-6 mt-5 mt-5">
          <h5> {maSP}</h5>
          <h5> {tenSP}</h5>
          <h5> {manHinh}</h5>
          <h5> {heDieuHanh}</h5>
          <h5> {cameraTruoc}</h5>
          <h5> {cameraSau}</h5>
          <h5> {ram}</h5>
          <h5> {rom}</h5>
          <h5> {giaBan}</h5>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    changePhone: state.phoneReducer.changePhone,
  };
};

export default connect(mapStateToProps)(DetailPhone);
