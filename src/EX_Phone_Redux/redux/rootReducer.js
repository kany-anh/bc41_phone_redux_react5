import { combineReducers } from "redux";
import { phoneReducer } from "./phoneReducer";

export const rootReducer_EX_Phone_Redux = combineReducers({
  phoneReducer,
});
