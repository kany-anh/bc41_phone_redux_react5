import React, { Component } from "react";
import { connect } from "react-redux";
import ItemPhone from "./ItemPhone";

class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.listPhone.map((item, index) => {
      return <ItemPhone data={item} key={index} />;
    });
  };
  render() {
    return (
      <div>
        <h2>List Phone</h2>
        <div className="container">
          <div className="row">{this.renderListPhone()}</div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    listPhone: state.phoneReducer.listPhone,
  };
};

export default connect(mapStateToProps, null)(ListPhone);
