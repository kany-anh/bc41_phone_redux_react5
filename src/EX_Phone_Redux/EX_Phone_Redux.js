import React, { Component } from "react";
import { connect } from "react-redux";
import { data_phone } from "./data_Phone";
import DetailPhone from "./DetailPhone";
import ListPhone from "./ListPhone";

class EX_Phone_Redux extends Component {
  state = {
    list: data_phone,
    phone: data_phone[0],
  };
  changePhone = (newPhone) => {
    this.setState({ phone: newPhone });
  };
  render() {
    return (
      <div>
        <ListPhone />
        <DetailPhone />
      </div>
    );
  }
}

export default EX_Phone_Redux;
