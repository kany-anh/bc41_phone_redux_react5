import React, { Component } from "react";
import { connect } from "react-redux";

class ItemPhone extends Component {
  render() {
    let { tenSP, giaBan, hinhAnh } = this.props.data;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary h-100">
          <img className="card-img-top" src={hinhAnh} alt="" />
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.handleChangePhone(this.props.data);
              }}
              className="btn btn-primary"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangePhone: (dataPhone) => {
      // const phone = {
      //   maSP: dataPhone.maSP,
      //   tenSP: dataPhone.tenSP,
      //   manHinh: dataPhone.manHinh,
      //   heDieuHanh: dataPhone.heDieuHanh,
      //   cameraTruoc: dataPhone.cameraTruoc,
      //   cameraSau: dataPhone.cameraSau,
      //   ram: dataPhone.ram,
      //   rom: dataPhone.rom,
      //   giaBan: dataPhone.giaBan,
      //   hinhAnh: dataPhone.hinhAnh,
      // };
      dispatch({
        type: "CHANGE_PHONE",
        payload: dataPhone,
      });
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemPhone);
