import "./App.css";
import EX_Phone_Redux from "./EX_Phone_Redux/EX_Phone_Redux";

function App() {
  return (
    <div className="App">
      <EX_Phone_Redux />
    </div>
  );
}

export default App;
